// Package p contains a Google Cloud Storage Cloud Function.
package p

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sort"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	vision "cloud.google.com/go/vision/apiv1"
	"github.com/pkg/errors"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
)

// GCSEvent is the payload of a GCS event. Please refer to the docs for
// additional information regarding GCS events.
type GCSEvent struct {
	Bucket string `json:"bucket"`
	Name   string `json:"name"`
}

// HelloGCS prints a message when a file is changed in a Cloud Storage bucket.
func HelloGCS(ctx context.Context, e GCSEvent) error {
	log.Printf("Processing file: %s", e.Name)
	return nil
}

// firestorePicture is the structure of a `pictures` document in Firestore
type firestorePicture struct {
	Labels []string `firestore:"labels"`
	Color  string   `firestore:"color"`
	// Created value will be set with the time of creation on the server (firestore) side
	// see https://godoc.org/cloud.google.com/go/firestore#DocumentRef.Create
	Created time.Time `firestore:"created,serverTimestamp"`
}

/* VisionAnalysis creates a thumbnail when a file is changed in a Cloud Storage bucket.
   You can deploy this function to Cloud Function with the command:
   gcloud functions deploy image-analysis \
	 --region $YOUR_REGION \
	 --entry-point VisionAnalysis \
	 --trigger-bucket $YOUR_SOURCE_BUCKET \
	 --runtime go111 \
	 --no-allow-unauthenticated
*/
func VisionAnalysis(ctx context.Context, e GCSEvent) error {
	log.Printf("Event: %#v", e)

	filename := e.Name
	filebucket := e.Bucket
	log.Printf("New picture uploaded %s in %s", filename, filebucket)

	client, err := vision.NewImageAnnotatorClient(ctx)
	if err != nil {
		log.Printf("Failed to create client: %v", err)
		return errors.New("Failed to create CloudVision client")
	}
	defer client.Close()

	request := &pb.AnnotateImageRequest{
		Image: &pb.Image{
			Source: &pb.ImageSource{
				ImageUri: fmt.Sprintf("gs://%s/%s", filebucket, filename),
			},
		},
		Features: []*pb.Feature{
			{Type: pb.Feature_LABEL_DETECTION},
			{Type: pb.Feature_IMAGE_PROPERTIES},
			{Type: pb.Feature_SAFE_SEARCH_DETECTION},
		},
	}

	r, err := client.AnnotateImage(ctx, request)
	if err != nil {
		log.Printf("Failed annotate image: %v", err)
		return fmt.Errorf("Vision API error: code %d, message: '%s'", r.Error.Code, r.Error.Message)
	}

	resp := visionResponse{r}
	log.Printf("Raw vision output for: %s: %s", filename, resp.toJSON())

	labels := resp.getLabels()
	log.Printf("Labels: %s", strings.Join(labels, ", "))

	color := resp.getDominantColor()
	log.Printf("Color: %s", color)

	if !resp.isSafe() {
		return nil
	}

	// if the picture is safe to display, store it in Firestore
	pictureStore, err := firestore.NewClient(ctx, firestore.DetectProjectID)
	if err != nil {
		log.Printf("Failed to get 'pictures' firestore collection: %v", err)
		return errors.New("Failed to get 'pictures' firestore collection")
	}

	_, err = pictureStore.Doc("pictures/"+filename).Create(ctx, firestorePicture{
		Labels: labels,
		Color:  color,
	})
	if err != nil {
		log.Printf("Failed to add picture in firestore: %v", err)
		return errors.New("Failed to add picture in firestore")
	}

	return nil
}

type visionResponse struct {
	*pb.AnnotateImageResponse
}

// toJSON returns a JSON representation of a response
func (o *visionResponse) toJSON() string {
	b, err := json.Marshal(o)
	if err != nil {
		return "## error marshalling data ##"
	}
	return string(b)
}

// byScore implements sort.Interface based on the Score field
type byScore []*pb.EntityAnnotation

func (o byScore) Len() int           { return len(o) }
func (o byScore) Swap(i, j int)      { o[i], o[j] = o[j], o[i] }
func (o byScore) Less(i, j int) bool { return o[i].Score > o[j].Score }

// getLabels returns the labels found in the response ordered by descending score
func (o *visionResponse) getLabels() (labels []string) {
	sort.Sort(byScore(o.LabelAnnotations))
	for _, label := range o.LabelAnnotations {
		labels = append(labels, label.Description)
	}
	return
}

// getDominantColor returns an Hex representation of the dominant color in the image
func (o *visionResponse) getDominantColor() (hex string) {
	var bestScore float32
	var bestColor *pb.ColorInfo
	for _, color := range o.ImagePropertiesAnnotation.DominantColors.Colors {
		if color.Score > bestScore {
			bestScore = color.Score
			bestColor = color
		}
	}
	if bestColor == nil {
		return "#ffffff"
	}
	return fmt.Sprintf("#%02x%02x%02x", int(bestColor.Color.Red), int(bestColor.Color.Green), int(bestColor.Color.Blue))
}

// isSafe returns true if no field of SafeSearchAnnotation is LIKELY or more
func (o *visionResponse) isSafe() bool {
	safe := o.SafeSearchAnnotation
	for _, value := range []*pb.Likelihood{&safe.Adult, &safe.Medical, &safe.Racy, &safe.Spoof, &safe.Violence} {
		if *value == pb.Likelihood_LIKELY || *value == pb.Likelihood_VERY_LIKELY {
			return false
		}
	}
	return true
}
