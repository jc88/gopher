package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

const (
	DateFormat = "2006-01-02"
	TimeFormat = "15:04:00"
)

type V2Order struct {
	ID          string  `json:"id"`
	ExternalID  string  `json:"external_id,omitempty"`
	ServiceType string  `json:"service_type"`
	Status      string  `json:"status"`
	OrderType   string  `json:"order_type"`
	CodCurrency string  `json:"cod_currency,omitempty"`
	CodValue    float64 `json:"cod_value,omitempty"`
	OrderModel  string  `json:"order_model,omitempty"`

	DeliveryType         string      `json:"delivery_type"`
	ExpectedDeliveryTS   V2Timeslots `json:"expected_delivery_ts"`
	DeliveryAddress      V2Address   `json:"delivery_address"`
	DeliveryInstructions string      `json:"delivery_instructions"`

	ExpectedPickupTS   V2Timeslots `json:"expected_pickup_ts"`
	PickUpInstructions string      `json:"pick_up_instructions"`
	PickUpAddress      V2Address   `json:"pickup_address"`

	ReturnInstructions string    `json:"return_instructions"`
	ReturnAddress      V2Address `json:"return_address"`

	Retailer     V2Retailer   `json:"retailer"`
	Customer     V2Customer   `json:"customer"`
	Packages     []V2Package  `json:"packages"`
	OrderDetails OrderDetails `json:"order_details"`
}

type V2Package struct {
	Barcode string `json:"barcode"`
	Height  string `json:"height"`
	Length  string `json:"length"`
	Weight  string `json:"weight"`
	Width   string `json:"width"`
}

type OrderDetails struct {
	RetailerOrderNumber string `json:"retailer_order_number"`
	PaackOrderNumber    string `json:"paack_order_number"`
}

type V2Address struct {
	Country string  `json:"country"`
	City    string  `json:"city"`
	ZipCode string  `json:"zip_code"`
	Line1   string  `json:"line1"`
	Line2   string  `json:"line2"`
	GeoCode GeoCode `json:"longitude"`
}

type GeoCode struct {
	GeoCode float32 `json:"longitude"`
	Phone   float32 `json:"latitude"`
}

type V2Retailer struct {
	ID      string `json:"id"`
	Name    string `json:"name"`
	StoreID string `json:"store_id"`
}

type V2Customer struct {
	FullName string    `json:"full_name"`
	Phone    string    `json:"phone"`
	Email    string    `json:"email"`
	Address  V2Address `json:"address"`
}

type V2Timeslots struct {
	EndTime   string `json:"end_time"`
	StartTime string `json:"start_time"`
}

type V3Order struct {
	ExternalID  string  `json:"external_id"`
	OrderID     string  `json:"order_id,omitempty"`
	GUID        string  `json:"guid,omitempty"`
	ParentID    string  `json:"parent_id,omitempty"`
	ServiceType string  `json:"service_type,omitempty"`
	RetailerID  string  `json:"retailer_id"`
	Status      string  `json:"status"`
	CodCurrency string  `json:"cod_currency"`
	CodValue    float64 `json:"cod_value"`
	WfID        string  `json:"wf_id,omitempty"`

	DeliveryInstructions string       `json:"delivery_instructions"`
	DeliveryAddress      V3Address    `json:"delivery_address"`
	ExpectedDeliveryTs   V3ExpectedTs `json:"expected_delivery_ts"`
	DeliveryType         string       `json:"delivery_type,omitempty"`
	DeliveryID           string       `json:"delivery_id,omitempty"`

	PickUpInstructions string       `json:"pick_up_instructions"`
	PickUpAddress      V3Address    `json:"pick_up_address"`
	ExpectedPickUpTs   V3ExpectedTs `json:"expected_pick_up_ts"`

	UndeliverableInstructions string    `json:"undeliverable_instructions,omitempty"`
	UndeliverableAddress      V3Address `json:"undeliverable_address"`

	InsuredCurrency string  `json:"insured_currency,omitempty"`
	InsuredValue    float64 `json:"insured_value,omitempty"`

	Customer V3Customer `json:"customer,omitempty"`
	Parcels  []V3Parcel `json:"parcels,omitempty"`

	RetailerName     string `json:"retailer_name,omitempty"`
	EditableUntil    string `json:"editable_until,omitempty"`
	PaackOrderNumber string `json:"paack_order_number"`
}

type V3Customer struct {
	Email          string    `json:"email,omitempty"`
	ExternalID     string    `json:"external_id,omitempty"`
	FirstName      string    `json:"first_name,omitempty"`
	LastName       string    `json:"last_name,omitempty"`
	Language       string    `json:"language,omitempty"`
	OrderRef       string    `json:"order_ref,omitempty"`
	Phone          string    `json:"phone,omitempty"`
	Address        V3Address `json:"address,omitempty"`
	HasGDPRConsent bool      `json:"has_gdpr_consent,omitempty"`
}

type V3Address struct {
	City     string `json:"city"`
	Line1    string `json:"line1"`
	PostCode string `json:"post_code"`
	Country  string `json:"country"`
	Line2    string `json:"line2,omitempty"`
	County   string `json:"county,omitempty"`
}

// NewV3Address is constructor
func NewV3Address(city, line1, postCode, country, line2 string) V3Address {
	return V3Address{
		City:     city,
		Line1:    line1,
		PostCode: postCode,
		Country:  country,
		Line2:    line2,
	}
}

type V3Parcel struct {
	Barcode          string  `json:"barcode,omitempty"`
	Height           float64 `json:"height,omitempty"`
	Length           float64 `json:"length,omitempty"`
	Weight           float64 `json:"weight,omitempty"`
	VolumetricWeight float64 `json:"volumetric_weight,omitempty"`
	Width            float64 `json:"width,omitempty"`
	ID               string  `json:"id,omitempty"`
}

type V3ExpectedTs struct {
	End   V3DateTime `json:"end"`
	Start V3DateTime `json:"start"`
}

type V3DateTime struct {
	Date string `json:"date"`
	Time string `json:"time"`
}

func NewV3DateTime(input string) (V3DateTime, error) {
	var dt V3DateTime

	dateTime, err := time.Parse(time.RFC3339, input)
	if err != nil {
		return V3DateTime{}, err
	}

	dt.Date = dateTime.UTC().Format(DateFormat)
	dt.Time = dateTime.UTC().Format(TimeFormat)

	return dt, nil
}

func (v2 V2Timeslots) ConvertToV3() (V3ExpectedTs, error) {
	var v3 V3ExpectedTs

	startDT, err := NewV3DateTime(v2.StartTime)
	if err != nil {
		return v3, err
	}

	endDT, err := NewV3DateTime(v2.EndTime)
	if err != nil {
		return v3, err
	}

	v3.Start = startDT
	v3.End = endDT

	return v3, nil
}

func (v2 V2Order) ConvertToV3() (*V3Order, error) {
	v3 := V3Order{}

	v3.ExternalID = v2.ExternalID
	// TODO add parent ID field
	// v3.OrderID
	// v3.ParentID
	v3.ServiceType = v2.ServiceType
	v3.RetailerID = v2.Retailer.ID
	v3.RetailerID = v2.Retailer.ID
	v3.Status = v2.Status
	v3.CodCurrency = v2.CodCurrency
	v3.CodValue = v2.CodValue

	v3.DeliveryInstructions = v2.DeliveryInstructions
	v3.DeliveryAddress = NewV3Address(
		v2.DeliveryAddress.City,
		v2.DeliveryAddress.Line1,
		v2.DeliveryAddress.ZipCode,
		v2.DeliveryAddress.Country,
		v2.DeliveryAddress.Line2,
	)

	dTs, err := v2.ExpectedDeliveryTS.ConvertToV3()
	if err != nil {
		return nil, err
	}

	v3.ExpectedDeliveryTs = dTs
	v3.DeliveryType = v2.DeliveryType

	v3.PickUpInstructions = v2.PickUpInstructions
	v3.PickUpAddress = NewV3Address(
		v2.PickUpAddress.City,
		v2.PickUpAddress.Line1,
		v2.PickUpAddress.ZipCode,
		v2.PickUpAddress.Country,
		v2.PickUpAddress.Line2,
	)

	dTs, err = v2.ExpectedPickupTS.ConvertToV3()
	if err != nil {
		return nil, err
	}

	v3.ExpectedPickUpTs = dTs

	v3.UndeliverableInstructions = v2.ReturnInstructions
	v3.UndeliverableAddress = NewV3Address(
		v2.ReturnAddress.City,
		v2.ReturnAddress.Line1,
		v2.ReturnAddress.ZipCode,
		v2.ReturnAddress.Country,
		v2.ReturnAddress.Line2,
	)

	v3.Customer = v2.Customer.ConvertV3()

	parcels := make([]V3Parcel, 0, len(v2.Packages))

	for i := range v2.Packages {
		var p V3Parcel

		p.Barcode = v2.Packages[i].Barcode
		// add all params

		parcels = append(parcels, p)
	}

	v3.Parcels = parcels

	v3.PaackOrderNumber = v2.OrderDetails.PaackOrderNumber

	return &v3, nil
}

func (c V2Customer) ConvertV3() V3Customer {
	var customer V3Customer
	customer.Phone = c.Phone
	customer.Email = c.Email

	fullName := strings.SplitN(c.FullName, " ", 2)
	customer.FirstName = fullName[0]
	customer.LastName = fullName[1]
	customer.Address = NewV3Address(
		c.Address.City,
		c.Address.Line1,
		c.Address.ZipCode,
		c.Address.Country,
		c.Address.Line2,
	)

	return customer
}

func main() {
	file, _ := ioutil.ReadFile("test.json")

	time.Parse(time.RFC3339, "asdas")

	data := V2Order{}

	_ = json.Unmarshal([]byte(file), &data)

	fmt.Println(data)

	data.ConvertToV3()
}
