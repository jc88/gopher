package main

import "testing"

type MyStruct struct {
	F1, F2, F3, F4, F5, F6, F7 string
	I1, I2, I3, I4, I5, I6, I7 int64
}

func BenchmarkAppendingStructs(b *testing.B) {
	var s []MyStruct

	for i := 0; i < b.N; i++ {
		s = append(s, MyStruct{})
	}
}

func BenchmarkAppendingPointers(b *testing.B) {
	var s []*MyStruct

	for i := 0; i < b.N; i++ {
		s = append(s, &MyStruct{})
	}
}

type MyStructAgain struct {
	A int
	B int
}

func BenchmarkSlicePointers(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		slice := make([]*MyStructAgain, 0, 100)
		for j := 0; j < 100; j++ {
			slice = append(slice, &MyStructAgain{A: j, B: j + 1})
		}
	}
}

func BenchmarkSliceNoPointers(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		slice := make([]MyStructAgain, 0, 100)
		for j := 0; j < 100; j++ {
			slice = append(slice, MyStructAgain{A: j, B: j + 1})
		}
	}
}

func BenchmarkSliceHybrid(b *testing.B) {
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		slice := make([]MyStructAgain, 0, 100)
		for j := 0; j < 100; j++ {
			slice = append(slice, MyStructAgain{A: j, B: j + 1})
		}
		slicep := make([]*MyStructAgain, len(slice))
		for j := range slice {
			slicep[j] = &slice[j]
		}
	}
}
