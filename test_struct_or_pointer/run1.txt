goos: darwin
goarch: amd64
pkg: gitlab.com/jc88/gopher/test_struct_or_pointer
cpu: Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz
BenchmarkAppendingStructs-12     	 2290731	       582.0 ns/op
BenchmarkAppendingStructs-12     	 3399039	       338.2 ns/op
BenchmarkAppendingStructs-12     	 2963588	       386.4 ns/op
BenchmarkAppendingStructs-12     	 3274851	       337.2 ns/op
BenchmarkAppendingStructs-12     	 3246915	       403.8 ns/op
BenchmarkAppendingStructs-12     	 3624901	       374.7 ns/op
BenchmarkAppendingStructs-12     	 3016584	       344.7 ns/op
BenchmarkAppendingStructs-12     	 3102010	       359.2 ns/op
BenchmarkAppendingStructs-12     	 3100777	       362.6 ns/op
BenchmarkAppendingStructs-12     	 2989339	       362.4 ns/op
BenchmarkAppendingPointers-12    	13747758	       112.0 ns/op
BenchmarkAppendingPointers-12    	16641127	       101.9 ns/op
BenchmarkAppendingPointers-12    	13893600	        73.30 ns/op
BenchmarkAppendingPointers-12    	16321992	        90.15 ns/op
BenchmarkAppendingPointers-12    	15633260	        78.68 ns/op
BenchmarkAppendingPointers-12    	15335668	        87.15 ns/op
BenchmarkAppendingPointers-12    	13584619	        81.45 ns/op
BenchmarkAppendingPointers-12    	16556364	        87.74 ns/op
BenchmarkAppendingPointers-12    	14573449	        85.78 ns/op
BenchmarkAppendingPointers-12    	15954864	        88.54 ns/op
BenchmarkSlicePointers-12        	  605754	      2031 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  599787	      2033 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  579223	      2022 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  575654	      2066 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  579307	      2006 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  582037	      2152 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  582781	      2045 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  585150	      2003 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  586630	      2011 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSlicePointers-12        	  582889	      2031 ns/op	    1600 B/op	     100 allocs/op
BenchmarkSliceNoPointers-12      	16165130	        72.49 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16491909	        74.33 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	15890724	        74.21 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16064348	        74.07 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16503476	        72.39 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16521219	        72.69 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16327884	        72.67 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16506156	        73.61 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	14133460	        76.74 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceNoPointers-12      	16669160	        73.47 ns/op	       0 B/op	       0 allocs/op
BenchmarkSliceHybrid-12          	 2044144	       573.7 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2089030	       579.8 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2078944	       577.6 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2079579	       576.1 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2078520	       586.1 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2038256	       579.6 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2096042	       593.9 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2069140	       574.2 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2093972	       586.6 ns/op	    2688 B/op	       2 allocs/op
BenchmarkSliceHybrid-12          	 2057295	       577.4 ns/op	    2688 B/op	       2 allocs/op
PASS
ok  	gitlab.com/jc88/gopher/test_struct_or_pointer	79.597s
