package main

import (
	"bytes"
	"context"
	"fmt"
	"io"

	"cloud.google.com/go/pubsub"
)

func publishCustomAttributes(w io.Writer, projectID, topicID string) error {
	// projectID := "my-project-id"
	// topicID := "my-topic"
	ctx := context.Background()
	client, err := pubsub.NewClient(ctx, projectID)
	if err != nil {
		return fmt.Errorf("pubsub.NewClient: %v", err)
	}
	defer client.Close()

	t := client.Topic(topicID)
	result := t.Publish(ctx, &pubsub.Message{
		Data: []byte("Hello world!"),
		Attributes: map[string]string{
			"origin":   "golang",
			"username": "gcp",
		},
	})
	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	id, err := result.Get(ctx)
	if err != nil {
		return fmt.Errorf("Get: %v", err)
	}
	fmt.Fprintf(w, "Published message with custom attributes; msg ID: %v\n", id)
	return nil
}

var qwe = `{
    "data" : {
        "attributes" : {
            "id": "f8752c93-8278-4040-b975-ca4bef235823",
                "paackOrderNumber": "",
                "parentId": "",
                "retailerId": "263246eb-e825-4527-a255-fd21d244ec2e",
                "numAttempts": 0,
                "status": "collection_missed",
                "codCurrency": "EUR",
                "codValue": 400,
                "insuredCurrency": "EUR",
                "insuredValue": 1,
                "customer": {
                    "id": "",
                    "address": {
                        "line1": "commodo culpa et in",
                        "line2": "Duis aliqua des",
                        "city": "Lorem",
                        "county": "irure voluptate labore nulla",
                        "postCode": "10003",
                        "country": "ES"
                    },
                    "geocode": null,
                    "firstName": "Bertram",
                    "lastName": "Gilfoyle",
                    "customerType": "standard",
                    "externalId": "labore minim",
                    "email": "",
                    "phone": "",
                    "language": "es",
                    "hasGDPRConsent": true,
                    "customerDetails": null,
                    "orderRef": ""
                },
                "deliveryType": "direct",
                "deliveryId": "",
                "deliveryAddress": {
                    "line1": "commodo culpa et in",
                    "line2": "Duis aliqua des",
                    "city": "Lorem",
                    "county": "irure voluptate labore nulla",
                    "postCode": "10003",
                    "country": "ES"
                },
                "deliveryInstructions": "amet cillum dolor",
                "expectedDeliveryTS": {
                    "start": "2021-02-09T16:09:53Z",
                    "end": "2021-02-09T16:09:53Z"
                },
                "expectedPickupTS": {
                    "start": "2021-02-09T16:09:53Z",
                    "end": "2021-02-09T16:09:53Z"
                },
                "dcId": "",
                "routeId": "",
                "clusters": null,
                "externalId": "d38bfe7g-9933-5b51-8821-807107014209",
                "parcels": [
                    {
                        "id": "f6b39ae5-212b-44f4-ac70-32b60a0de5f8",
                        "orderId": "f8752c93-8278-4040-b975-ca4bef235823",
                        "barcode": "8769485769",
                        "height": 84,
                        "length": 33,
                        "volumetricWeight": 3475,
                        "weight": 83,
                        "width": 22,
                        "type": "standard",
                        "status": "status1",
                        "details": null,
                        "location": null
                    },
                    {
                        "id": "324bbcbf-bab6-4d37-9c46-285346829718",
                        "orderId": "f8752c93-8278-4040-b975-ca4bef235823",
                        "barcode": "98273596863",
                        "height": 24,
                        "length": 43,
                        "volumetricWeight": 3885,
                        "weight": 93,
                        "width": 82,
                        "type": "standard",
                        "status": "status1",
                        "details": null,
                        "location": null
                    }
                ],
                "orderDetails": null,
                "pickUpAddress": {
                    "line1": "commodo culpa et in",
                    "line2": "Duis aliqua des",
                    "city": "Lorem",
                    "county": "irure voluptate labore nulla",
                    "postCode": "10003",
                    "country": "ES"
                },
                "pickUpInstructions": "voluptate",
                "serviceType": "ST2",
                "undeliverableAddress": {
                    "line1": "commodo culpa et in",
                    "line2": "Duis aliqua des",
                    "city": "Lorem",
                    "county": "irure voluptate labore nulla",
                    "postCode": "10003",
                    "country": "ES"
                },
                "undeliverableInstructions": "",
                "trackingURL": "",
                "trackingId": "",
                "location": null
        }
    }
}`

func main() {
	var buf bytes.Buffer
	// projectID := "squad01-25e4"
	projectID := "squad02-1483"
	// topicID := "projects/squad01-25e4/topics/eventarc-europe-west1-events-pubsub-trigger-866"
	topicID := "projects/squad02-1483/topics/jagrmi"

	publishCustomAttributes(&buf, projectID, topicID)

}
