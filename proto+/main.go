package main

import (
	"fmt"

	"gitlab.com/jc88/gopher/proto+/go/example.com/personpb"
)

func main() {
	usePerson()
}

func usePerson() {
	joey := personpb.Person{
		FistName:   "Joey",
		LastName:   "Tribiyani",
		Age:        30,
		IsVerified: true,
	}
	// print Person struct
	fmt.Println(joey)
	// modify the age of person
	joey.Age = 35
	// print Person struct
	fmt.Println(joey)

	//get First Name using getter method.
	fmt.Println("Persons First Name:", joey.GetFistName())
}
