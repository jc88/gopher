



## Generate code for Proto


`? proto-example `

`|_ ? proto    `

    `|_ Person.proto `

`|_ ? java `

`|_ ? go`



## JAVA

// for MacOS and Linux
protoc -I=proto/ --java_out=java proto/Person.proto


## GOLANG


<pre><pre><code class=" hljs bash hljs ">option go_package = "example.com/pkg";</code></pre></pre>


`syntax = "proto3";

option go_package = "example.com/pkg";

message Person {
   string fist_name = 1;
   string last_name = 2;
   int32 age = 3;
   bool is_verified = 4;
}`



// for MacOS and Linux
protoc -I=proto/ --go_out=go proto/Person.proto


## PYTHON

// for MacOS and Linux
protoc -I=proto/ --python_out=python proto/Person.proto
