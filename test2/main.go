package main

import (
	"fmt"
	"strconv"
	"strings"
)

func q1(a []int) {
	var returnDays []string
	for _, day := range a {
		returnDays = append(returnDays, strconv.Itoa(day))
	}
	fmt.Println(strings.Join(returnDays, ","))
}

func arrayToString(a []int, delim string) string {
	return strings.Trim(strings.Replace(fmt.Sprint(a), " ", delim, -1), "[]")
}

func main() {
	x1 := []int{1, 2, 3}

	q1(x1)

	fmt.Println(arrayToString(x1, ","))

	fmt.Println(fmt.Sprint(x1))
}
