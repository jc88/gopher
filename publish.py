import os
import json

data_set = {
    "id": "4db150da-5f7a-40f7-83b7-9c7717b4e15b",
	"paackOrderNumber": "",
	"parentId": "4db150da-5f7a-40f7-83b7-9c7717b4e15b",
	"retailerId": "263246eb-e825-4527-a255-fd21d244ec2e",
    "numAttempts": 0,
    "status": "Manifested",
    "codCurrency": "EUR",
    "codValue": 400,
    "insuredCurrency": "EUR",
    "insuredValue": 1,
    "customer": {
        "id": "",
        "address": {
            "line1": "commodo culpa et in",
            "line2": "Duis aliqua des",
            "city": "Lorem",
            "county": "irure voluptate labore nulla",
            "postCode": "10003",
            "country": "ES"
        },
        "geocode": None,
        "firstName": "Bertram",
        "lastName": "Gilfoyle",
        "customerType": "standard",
        "externalId": "labore minim",
        "email": "gilfoyle@piedpiper.com",
        "phone": "+34666666666",
        "language": "es",
        "hasGDPRConsent": True,
        "customerDetails": None,
        "orderRef": "12381023"
    },
    "deliveryType": "direct",
    "deliveryId": "",
    "deliveryAddress": {
        "line1": "commodo culpa et in",
        "line2": "Duis aliqua des",
        "city": "Lorem",
        "county": "irure voluptate labore nulla",
        "postCode": "10003",
        "country": "ES"
    },
    "deliveryInstructions": "amet cillum dolor",
    "expectedDeliveryTS": {
        "start": "2021-02-09T16:09:53Z",
        "end": "2021-02-09T16:09:53+01:00"
    },
    "expectedPickupTS": {
        "start": "2021-02-09T16:09:53Z",
        "end": "2021-02-09T16:09:53Z"
    },
    "dcId": "",
    "routeId": "",
    "clusters": None,
    "externalId": "d38bfe7g-9943-5b51-8821-807107014211",
    "parcels": [
        {
            "id": "a0c313b7-b884-4b5f-b340-f45d44b48ad0",
            "orderId": "4db150da-5f7a-40f7-83b7-9c7717b4e15b",
            "barcode": "8769485769",
            "height": 84,
            "length": 33,
            "volumetricWeight": 3475,
            "weight": 83,
            "width": 22,
            "type": "standard",
            "status": "Manifested",
            "details": None,
            "location": None
        },
        {
            "id": "5c1816e1-99b5-4715-af93-630f6f32da47",
            "orderId": "4db150da-5f7a-40f7-83b7-9c7717b4e15b",
            "barcode": "98273596863",
            "height": 24,
            "length": 43,
            "volumetricWeight": 3885,
            "weight": 93,
            "width": 82,
            "type": "standard",
            "status": "Manifested",
            "details": None,
            "location": None
        }
    ],
    "orderDetails": None,
    "pickUpAddress": {
        "line1": "commodo culpa et in",
        "line2": "Duis aliqua des",
        "city": "Lorem",
        "county": "irure voluptate labore nulla",
        "postCode": "10003",
        "country": "ES"
    },
    "pickUpInstructions": "voluptate",
    "serviceType": "ST2",
    "undeliverableAddress": {
        "line1": "commodo culpa et in",
        "line2": "Duis aliqua des",
        "city": "Lorem",
        "county": "irure voluptate labore nulla",
        "postCode": "10003",
        "country": "ES"
    },
    "undeliverableInstructions": "",
    "trackingURL": "",
    "trackingId": "",
    "location": None
}

re_fad = {
    "basic": {
        "id": "60794549-9ddb-429f-abf7-98992747ed04",
        "parent_id": "cae66b85-2f93-4177-b256-eeee5b46fd7a",
        "name": "cms.v1.order.scanned_at_origin",
        "at": "2021-02-24T20:20:49+00:00",
        "aggregate_root_id": "92ac3c0c-f4b0-4085-bb98-7f57827eb064"
    },
    "order_id": "92ac3c0c-f4b0-4085-bb98-7f57827eb064",
    "external_id": "RandoOrderNumber",
    "retailer_id": "b286e926-f4ad-47e0-9300-e2812e3b0366",
    "updated_at": "2021-02-24T20:20:49+00:00"
}

def publish_messages(project_id: str, topic_id: str) -> None:
    """Publishes multiple messages to a Pub/Sub topic."""
    # [START pubsub_quickstart_publisher]
    # [START pubsub_publish]
    from google.cloud import pubsub_v1

    # TODO(developer)
    # project_id = "your-project-id"
    # topic_id = "your-topic-id"

    publisher = pubsub_v1.PublisherClient()
    # The `topic_path` method creates a fully qualified identifier
    # in the form `projects/{project_id}/topics/{topic_id}`
    topic_path = publisher.topic_path(project_id, topic_id)

    # for n in range(1, 10):
    #     data_str = f"Message number {n}"
    #     # Data must be a bytestring
    #     data = data_str.encode("utf-8")
    #     # When you publish a message, the client returns a future.
    #     future = publisher.publish(topic_path, data)
    #     print(future.result())

    # data_str = f"Message number {n}"
    # Data must be a bytestring
    # json_dump = json.dumps(data_set)

    # re-fad
    json_dump = json.dumps(re_fad)

    data = json_dump.encode("utf-8")
    # When you publish a message, the client returns a future.
    future = publisher.publish(topic_path, data)
    print(future.result())

    print(f"Published messages to {topic_path}.")
    # [END pubsub_quickstart_publisher]
    # [END pubsub_publish]

if __name__ == "__main__":
    project_id = "squad02-1483"
    topic_id = "jagrmi"

    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/andrei.ramanchyk/paack/squad02-1483-ea2c6647cdf8.json"

    publish_messages(project_id, topic_id)
    pass
