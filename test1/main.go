package main

import (
	"context"
	"fmt"
	"log"
	"time"
)

func main() {
	main2()

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	go someLongRunningTaskOne(ctx)
	go someLongRunningTaskTwo(ctx)

	time.Sleep(time.Second * 5)
}

func someLongRunningTaskOne(ctx context.Context) {
	time.Sleep(time.Second * 3)
	fmt.Println("func 1")
	ctx.Done()
}

func someLongRunningTaskTwo(ctx context.Context) {
	time.Sleep(time.Second * 2)
	fmt.Println("func 2")

	ctx.Done()

	// ctx.
}

func main2() {
	go func() {
		go func() {
			go func() {
				go func() {
					go func() {
						go func() {
							go func() {
								log.Println("maybe I should have thought about this program a little more")
							}()
						}()
					}()
				}()
			}()
		}()
	}()
	time.Sleep(time.Second)
}
